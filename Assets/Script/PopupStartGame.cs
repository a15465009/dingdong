using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupStartGame : MonoBehaviour
{
    [SerializeField] private Button btnStart;
    void Start()
    {
        GameManager.Instance.StartGame += ClickStart;
        btnStart.onClick.AddListener(() => { GameManager.Instance.StartGame.Invoke(); });
         
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void ClickStart()
    {
        transform.DOMove(new Vector2(6, 0), 1);

    }
}
