using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnviromentManager : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed = 3;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Test Rotation
        if (Input.GetKeyDown(KeyCode.R))
        {
            Rotate();
        }
    }
    private void Rotate()
    {
        transform.DORotate(new Vector3(0, 0, 20), speed).SetEase(Ease.Linear).OnComplete(
            SwapRotate
            );
    }
    private void SwapRotate()
    {
        transform.DORotate(new Vector3(0, 0, -20), speed).SetEase(Ease.Linear).OnComplete(ResetRotate);
    }
    private void ResetRotate()
    {
        transform.DORotate(Vector3.zero,2);
    }
}
