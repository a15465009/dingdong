using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ObtacleController : MonoBehaviour
{
    private Transform target;
    private float speed = 3;
    void Start()
    {
        
    }


    void Update()
    {
        if(transform.position == target.position || GameManager.Instance.GetCheckOver())
        {
            Destroy(gameObject);
            ObtacleManager.Instance.CountSpawn(1);
            ObtacleManager.Instance.CheckCountSpawn();
        }
        transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
    }
    public void Init(Transform posSpawn, Transform posTarget)
    {
        target = posTarget;
        transform.position = posSpawn.position;
    }

}
