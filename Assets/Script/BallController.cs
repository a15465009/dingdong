using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform _target1;
    public Transform _target2;
    public float speed;
    private float speedbase;
    private bool up;
    private bool immortal;

    void Start()
    {
        immortal = true;
        speedbase = speed;
        up = true;
       // GameManager.Instance.CheckOver += EndGame;
       
    }
    private void OnEnable()
    {
        
    }
    private void OnDisable()
    {
       
    }
   
    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.GetCheckOver())
            return;
        
        if (Input.GetMouseButton(0))
        {
            speed -= Time.deltaTime;
            if(speed <= 0.2f)
            {
                speed = 0.2f;
            }
        }
        if (Input.GetMouseButtonUp(0)){
            speed = speedbase;
        }

        if (up)
        {
            transform.position = Vector3.MoveTowards(transform.position, _target1.position, speed * Time.deltaTime);
            
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, _target2.position, speed * Time.deltaTime);
           
        }
    }  
    private void OnCollisionEnter2D(Collision2D collision)
    {
       
       
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "pos1")
        {
            up = false;
            transform.localScale = new Vector3(transform.localScale.x, -transform.localScale.y, transform.localScale.z);
            PointManager.Instance.ActionPoint.Invoke();
        }
        if (collision.gameObject.tag == "pos2")
        {
            up = true;
            transform.localScale = new Vector3(transform.localScale.x, -transform.localScale.y, transform.localScale.z);
            PointManager.Instance.ActionPoint.Invoke();
        }
        if (collision.gameObject.tag == "obstacle")
        {
            // End game
            // if ball have immortal, dont die 1 times
            if (immortal)
            {
                immortal = false;
                Destroy(collision.gameObject);
                ObtacleManager.Instance.CountSpawn(1);
                ObtacleManager.Instance.CheckCountSpawn();
            }
            else
            {
                GameManager.Instance.CheckOver.Invoke();
            }

        }
    }
    
   
    

}
