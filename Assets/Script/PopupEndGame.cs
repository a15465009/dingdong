using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PopupEndGame : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private Button ButtonClose;
    [SerializeField] private Button ButtonContinue;
    [SerializeField] private TextMeshProUGUI txtPoint;
    void Start()
    {
        ButtonClose.onClick.AddListener(() => {
            ClickButtonClose();
        });
        ButtonContinue.onClick.AddListener(() => { ClickContinue(); });
        PointManager.Instance.ActionPoint += UpdatePoint;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void ClickButtonClose()
    {
        transform.DOMove(new Vector2(-8, 0), 0.5f).SetEase(Ease.Linear);
        GameManager.Instance.StartGame.Invoke();
    }
    private void ClickContinue()
    {
        transform.DOMove(new Vector2(-8, 0),0.5f).SetEase(Ease.Linear);
        GameManager.Instance.ContinueGame.Invoke();
    }
    void UpdatePoint()
    {
        txtPoint.text = PointManager.Instance.GetPoint().ToString();
    }
}
