using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class PointManager : MonoBehaviour
{
    // Start is called before the first frame update
    public static PointManager Instance;
    public Action ActionPoint;
    [SerializeField] private TextMeshProUGUI txtPoint;
    private int Point;
    void Start()
    {
        Point = 0;
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        ActionPoint += AddPoint;
        GameManager.Instance.StartGame += ResetPoint;
       // GameManager.Instance.ContinueGame += ResetPoint;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnEnable()
    {
        
    }
    public void AddPoint()
    {
        Point++;
        SetTextPoint();
    }
    public int GetPoint()
    {
        return Point;
    }
    public void ResetPoint()
    {
        Point = 0;
        SetTextPoint() ;
    }
    private void SetTextPoint()
    {
        txtPoint.text = Point.ToString();
    }
}
