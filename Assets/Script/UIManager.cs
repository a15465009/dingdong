using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    #region Skeleton
    [SerializeField] private GameObject PopupEnd;
    #endregion
    void Start()
    {
        GameManager.Instance.CheckOver += CheckPopupEnd;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnEnable()
    {
       
    }
    private void OnDisable()
    {
       GameManager.Instance.CheckOver -= CheckPopupEnd;
    }
    #region Function

    private void CheckPopupEnd()
    {    
            PopupEnd.transform.DOMove(Vector3.zero, 0.5f).SetEase(Ease.Linear);    
    }
    #endregion
}
