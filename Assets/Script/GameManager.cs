using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public static GameManager Instance;
    public Action CheckOver;
    public Action StartGame;
    public Action ContinueGame;
    private bool GameOver;
    public bool GetCheckOver()
    {
        return GameOver;
    }
    public void SetGameOver(bool over)
    {
        GameOver = over;
    }
    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        StartGame += FuncStartGame;
        CheckOver += FuncGameOver;
        ContinueGame += FuncCointinueGame;
        GameOver = true;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    private void FuncGameOver()
    {
       SetGameOver(true);
    }
    private void FuncStartGame()
    {
        // Star
        SetGameOver(false);
    }
    private void FuncCointinueGame()
    {
        // Star
        SetGameOver(false);
    }
}
