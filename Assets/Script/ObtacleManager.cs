using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObtacleManager : MonoBehaviour
{
    public static ObtacleManager Instance;
    [SerializeField] private List<ObtacleController> listObstacle;
    public List<Transform> ListposLeft;
    public List<Transform> ListposRight;
    [SerializeField] private int NumberObstacle;

    [SerializeField] private int Count = 0;
    private bool DeplayStart;
    [SerializeField] private float timeDeplayStart;
    public void CountSpawn(int count)
    {
        Count += count;
    }
    [SerializeField] private bool Spawn;
    public void SetSpawn(bool setSpawn)
    {
        Spawn = setSpawn;
    }
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        GameManager.Instance.CheckOver += CheckOver;
        GameManager.Instance.StartGame += StartSpawn;
        GameManager.Instance.ContinueGame += StartSpawn;
        DeplayStart = false;
    }

    
    void Update()
    {
         
    }
    private void SpawnObstacle(Transform posSpawn, Transform posTarget)
    {
        int ran = Random.RandomRange(0, listObstacle.Count);
        var ob = Instantiate<ObtacleController>(listObstacle[ran], transform);
        ob.Init(posSpawn,posTarget);
    }
    private void PickPosSpawn(int num)
    {

        for (int i = 0; i < num; i++)
        {
            int ran = Random.Range(0, 2);
            var listTarget = new List<Transform>();
            var listSpawn = new List<Transform>();
            if (ran == 0)
            {
                //choose list 1
                listSpawn = ListposRight;
                listTarget = ListposLeft;
            }
            else
            {
                //choose list 2
                listSpawn = ListposLeft;
                listTarget = ListposRight;

            }
            int ran2 = Random.Range(0, listTarget.Count);
            SpawnObstacle(listSpawn[ran2], listTarget[ran2]);
        }     
    }
    IEnumerator DeplaySpawn()
    {
        if (DeplayStart)
        {
            yield return new WaitForSeconds(timeDeplayStart);
            DeplayStart = false;
        }
        //Add Number Obtacle befor rung PickPosSpawn
        PickPosSpawn(NumberObstacle);
        Spawn = false;
        yield return new WaitUntil(() => Spawn == true);
        StartCoroutine(DeplaySpawn());
    }
    private void CheckOver()
    {
        StopAllCoroutines();
    }
    private void StartSpawn()
    {
        DeplayStart = true;
        StartCoroutine(DeplaySpawn());
    }
    private void ResetNumObstacle() {
        NumberObstacle = 1;
    }
    private void AddNumberObstacle()
    {
        NumberObstacle++;
    }
    public void CheckCountSpawn()
    {
        if(Count == NumberObstacle)
        {
            Spawn = true;
            Count = 0;
        }
    } 

}
